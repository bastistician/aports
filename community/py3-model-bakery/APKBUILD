# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-model-bakery
_pyname="model_bakery"
pkgver=1.17.0
pkgrel=0
# s390x: missing ruff checkdepend
arch="noarch !s390x"
pkgdesc="Smart object creation facility for Django"
url="https://pypi.python.org/project/model-bakary"
license="Apache-2.0"
depends="py3-django"
makedepends="
	py3-setuptools
	py3-gpep517
	py3-wheel
	py3-hatchling
	"
checkdepends="
	black
	py3-pytest-django
	py3-coverage
	py3-pillow
	py3-mypy
	ruff
	"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/model-bakers/model_bakery/archive/refs/tags/$pkgver.tar.gz
	"
builddir="$srcdir"/$_pyname-$pkgver
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -v
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}
sha512sums="
eaa7be3432842eb68035b9dcda5184befe4e7db51af4adbc92ae43e19670d0236a800dc61f12f23678e85be0abb80d68139c7a63e14b53f7631216643311fcbf  py3-model-bakery-1.17.0.tar.gz
"
